---
layout: default_without_pic
title: Catoooorse, el programa de rol
---

# Catoooorse: el programa de rol

<p class="center"><img src="/img/catoooorse_logo.jpg" alt="Catoooorse" /></p>

Catoooorse es un podcast de rol que intentará hacerse cada 15 dias (cada dos semanas), en el cual hablare sobre diferentes partidas de rol que ando jugando, sistemas nuevos que hemos visto y muchos otros temas de conversacion que pueden surgir. Tambien recibiremos diferentes invitados hablando sobre distintas situaciones dentro de una partida de rol y divagando un poco en que es lo que nos gusta del rol y por que lo jugamos.

<a href="https://podcasts.apple.com/us/podcast/catoooorse/id1619770613" target="_blank"><img src="img/itunes.png" alt="Link de Catoooorse en iTunes" /></a>
<a href="https://open.spotify.com/show/1mcYUhrQ23onsDOIMQ07Ud" target="_blank"><img src="img/spotify.png" alt="Link de Catoooorse en Spotify" /></a>
<a href="https://www.youtube.com/playlist?list=PL6qP8jQt3wc6FPKMIIEF53dW8W4CZrvPd" target="_blank"><img src="img/youtube.png" alt="Link de Catoooorse en YouTube" /></a>