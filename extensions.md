---
layout: default_without_pic
title: Extensiones
---

# Extensiones

Para mejorar la experiencia dentro del streaming, te recomiendo las siguientes extensiones:

## Twitch Pronouns
Sirve para especificar tus pronombres dentro del chat de Twitch.  
[Pagina para configurar y descargar la extension](https://pronouns.alejo.io/)

## BetterTTV
Mejora todo lo relacionado con Twitch: emotes fuera de la plataforma de Twitch, mejoras en el chat, mejoras visuales,...  
[Pagina principal](https://betterttv.com/)

## Disable Twitch Extension
Elimina todas las extensiones de Twitch por defecto, con un menu para permitir y bloquear extensiones dependiendo de tus gustos  
[Pagina principal](https://twitch-tools.rootonline.de/disable_twitch_extensions.php)