---
layout: default_without_pic
title: Reference Sheet
---

<style>
img {
    max-height: 400px;
}

table {
    margin: 0 auto;
    font-size: 24px;
}

.text {
    text-align: center;
}

.center {
    width: 100%;
}

.color-sample-box {
    width: 20px;
    height: 20px;
    content: '';
    /* display: inline; */
}

.color-sample-box-purple {
    background-color: #701dc5;
}

.color-sample-box-blue {
    background-color: #2484d6;
}

.color-sample-box-yellow {
    background-color: #fff8ee;
}

.color-sample-box-pink {
    background-color: #f4c1bf;
}
</style>

# Reference Sheet

## Description

Masaki Sendo, Capi, Saki or Capisaki is a capybara that is tall, about 180cm. He's a regular capybara, with a darker brown skin.

Saki is always wearing either baggy or thin clothes. His go-to shirt is his pink flamingo shirt, but he likes his axolotl sweater too. He doesn't like pants too much, that's why he wears shorts or sweatpants. Also, he likes black sneakers!

## Colors

Main colors (backgrounds and assets)

<table>
<tbody>
<tr>
<td>
<div class="color-sample-box color-sample-box-purple"></div>
</td>
<td>
#701dc5
</td>
</tr>
<tr>
<td>
<div class="color-sample-box color-sample-box-blue"></div>
</td>
<td>
#2484d6
</td>
</tr>
</tbody>
</table>

Shirt colors

<table>
<tbody>
<tr>
<td>
<div class="color-sample-box color-sample-box-yellow"></div>
</td>
<td>
#fff8ee
</td>
</tr>
<tr>
<td>
<div class="color-sample-box color-sample-box-pink"></div>
</td>
<td>
#f4c1bf
</td>
</tr>
</tbody>
</table>

## Images

The following images are copyrighted by me and the artists that did the pieces. You are not allowed to use it commercially, unless explicited or with permission.  
You can click in them and it will open in a new tab, with its biggest resolution available.

<table>
<tbody>
<tr>
<td>
Regular pic
</td>
<td>
Winter variant 2021/2022
</td>
</tr>
<tr>
<td>
<a href="img/profile.png" target="_blank"><img src="img/profile.png" alt="Profile pic by @kit_katei" /></a>
</td>
<td>
<a href="img/winter21.png" target="_blank"><img src="img/winter21.png" alt="Winter variant by @kit_katei" /></a>
</td>
</tr>
<tr>
<td>
<a href="img/full_shirt_front_2048.png" target="_blank"><img src="img/full_shirt_front_2048.png" alt="Fullbody shirt front pic by @kit_katei" /></a>
<a href="img/full_shirt_back_2048.png" target="_blank"><img src="img/full_shirt_back_2048.png" alt="Fullbody shirt back pic by @kit_katei" /></a>
<a href="img/full_shirt_open_open_2048.png" target="_blank"><img src="img/full_shirt_open_open_2048.png" alt="Fullbody shirt front pic by @kit_katei" /></a>
</td>
<td>
<a href="img/full_sweater_front_2048.png" target="_blank"><img src="img/full_sweater_front_2048.png" alt="Fullbody sweater front pic by @kit_katei" /></a>
<a href="img/full_sweater_back_2048.png" target="_blank"><img src="img/full_sweater_back_2048.png" alt="Fullbody sweater back pic by @kit_katei" /></a>
<a href="img/full_sweater_open_closed_2048.png" target="_blank"><img src="img/full_sweater_open_closed_2048.png" alt="Fullbody sweater front pic by @kit_katei" /></a>
</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
<td>
Profile pic, happy and mad reactions
</td>
</tr>
<tr>
<td>
<a href="img/profile_pic.png" target="_blank"><img src="img/profile_pic.png" alt="Profile pic by @kit_katei" /></a>
<a href="img/sakiHappy.png" target="_blank"><img src="img/sakiHappy.png" alt="Happy by @kit_katei" /></a>
<a href="img/sakiMad.png" target="_blank"><img src="img/sakiMad.png" alt="Mad by @kit_katei" /></a>
</td>
</tr>
</tbody>
</table>

Art by [@kit_katei](https://twitter.com/kit_katei)

Flying Saki

[![Flying Saki by @Akur_VR](img/flyingcapiizq.png)](img/flyingcapiizq.png){:target="_blank"}

Art by [@Akur_VR](https://twitter.com/Akur_VR)

Life is not daijoubu

[![Life is not daijoubu by @eleotaku_iguess](img/lifeisnotdaijoubu.jpg)](img/lifeisnotdaijoubu.jpg){:target="_blank"}

Art by [@eleotaku_iguess](https://twitter.com/eleotaku_iguess)

Masaki hacking

[![Masaki hacking by @mugitchi](img/masaki_hacking.png)](img/masaki_hacking.png){:target="_blank"}

Art by [@mugitchi](https://twitter.com/mugitchi)