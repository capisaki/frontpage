---
layout: default
title: Homepage
---

# MASAKI SENDO

Hola! Soy Masaki Sendo, tambien conocido en internet por capisaki o simplemente Saki!

Soy un VTuber capibara que por el dia soy desarrollador web y por las tardes y noches me gusta jugar a videojuegos y a juegos de mesa.

---

Pronombres: él/he/him.  
Color favorito: azul claro o azul cyan, depende del día.  
Me gusta: café, chocolate, fotografía, lofi.  
No me gusta: tabaco, música estridente, no dormir mis 8h.  

Juegos favoritos: VA-11 HALL-A, TLoZ: BotW, Pokémon Esmeralda, Forza Horizon 4.
