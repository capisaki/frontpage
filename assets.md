---
layout: default_without_pic
title: Assets
---

<style>
img {
    max-height: 400px;
}

.center {
    text-align: center;
}
</style>

# Assets

## Emote Wallpaper

This wallpaper has been made using commisioned art and modified with permission by [@kit_katei](https://twitter.com/kit_katei) and [@ELAIK_ART](https://twitter.com/ELAIK_ART).  
By no means, you can use this art and claim it as yours, nor use it for commercial purposes.

### Purple Edition
<div class="center">
    <a href="img/wallpapers/emotes_purple/capisaki_emote_purple_1080.jpg" target="_blank"><img src="img/wallpapers/emotes_purple/capisaki_emote_purple_1080.jpg" alt="Emote Wallpaper Purple Edition" /></a>
</div>

#### Screen Resolution
<div class="center">
    <a href="img/wallpapers/emotes_purple/capisaki_emote_purple_720.jpg" target="_blank">1280x720</a>
    <a href="img/wallpapers/emotes_purple/capisaki_emote_purple_1080.jpg" target="_blank">1920x1080</a>
    <a href="img/wallpapers/emotes_purple/capisaki_emote_purple_1440.jpg" target="_blank">2560x1440</a>
</div>
<div class="center">
    <a href="img/wallpapers/emotes_purple/capisaki_emote_purple_1336.jpg" target="_blank">1024x1366 (iPad)</a>
    <a href="img/wallpapers/emotes_purple/capisaki_emote_purple_2280.jpg" target="_blank">1080x2280 (Mobile)</a>
    <a href="img/wallpapers/emotes_purple/capisaki_emote_purple_2960.jpg" target="_blank">1440x2960 (Mobile)</a>
</div>

### Blue Edition
<div class="center">
    <a href="img/wallpapers/emotes_blue/capisaki_emote_blue_1080.jpg" target="_blank"><img src="img/wallpapers/emotes_blue/capisaki_emote_blue_1080.jpg" alt="Emote Wallpaper Blue Edition" /></a>
</div>

#### Screen Resolution
<div class="center">
    <a href="img/wallpapers/emotes_blue/capisaki_emote_blue_720.jpg" target="_blank">1280x720</a>
    <a href="img/wallpapers/emotes_blue/capisaki_emote_blue_1080.jpg" target="_blank">1920x1080</a>
    <a href="img/wallpapers/emotes_blue/capisaki_emote_blue_1440.jpg" target="_blank">2560x1440</a>
</div>
<div class="center">
    <a href="img/wallpapers/emotes_blue/capisaki_emote_blue_1336.jpg" target="_blank">1024x1366 (iPad)</a>
    <a href="img/wallpapers/emotes_blue/capisaki_emote_blue_2280.jpg" target="_blank">1080x2280 (Mobile)</a>
    <a href="img/wallpapers/emotes_blue/capisaki_emote_blue_2960.jpg" target="_blank">1440x2960 (Mobile)</a>
</div>

